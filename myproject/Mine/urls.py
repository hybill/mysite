from django.conf.urls import url
from . import views
app_name = 'Mine'
urlpatterns = [
    url(r'^Mine/$', views.IndexView.as_view(), name='home'),
    url(r'^Algorithm/$', views.AlgorithmListView.as_view(), name='Algorithm'),
]