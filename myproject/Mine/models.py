from django.db import models

# Create your models here.
from django.db import models

class programming_language_tags(models.Model):
    name = models.CharField(max_length=100)
    def __str__(self):
        return self.name

class tech_stack_tags(models.Model):
    name = models.CharField(max_length=100)
    def __str__(self):
        return self.name

class development_tools_tags(models.Model):
    name = models.CharField(max_length=100)
    def __str__(self):
        return self.name

class Projects(models.Model):
    name = models.CharField(max_length=100)
    def __str__(self):
        return self.name
    create_time = models.DateField()
    description = models.TextField()
    programming_language = models.ManyToManyField(programming_language_tags,blank=True)
    tech_stack = models.ManyToManyField(tech_stack_tags,blank=True)
    development_tools = models.ManyToManyField(development_tools_tags,blank=True)
    details = models.TextField()


class Algorithm(models.Model):
    name = models.CharField(max_length=100)
    create_time = models.DateField()
    def __str__(self):
        return self.name
    code = models.TextField()
