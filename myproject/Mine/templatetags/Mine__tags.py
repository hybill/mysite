from ..models import Projects,tech_stack_tags,programming_language_tags,development_tools_tags
from django import template
from django.db.models.aggregates import Count

register = template.Library()

@register.simple_tag
def get_programming_language_tags():
    return programming_language_tags.objects.all()

@register.simple_tag
def get_tech_stack_tags():
    return tech_stack_tags.objects.all()

@register.simple_tag
def get_development_tools_tags():
    return development_tools_tags.objects.all()