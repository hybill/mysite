from django.shortcuts import render

# Create your views here.
from  django.shortcuts import render,get_object_or_404
from django.views.generic import ListView
from  .models import Projects,Algorithm
import markdown

class IndexView(ListView):
    model = Projects
    template_name = 'projects/index.html'
    context_object_name = 'Projects_list'
    def get_queryset(self):
        return super(IndexView, self).get_queryset().order_by("-create_time")


class AlgorithmListView(ListView):
    model = Algorithm
    template_name = 'projects/Algorithm_list.html'
    context_object_name = 'Algorithm_list'
    def get_queryset(self):
        posts = super(AlgorithmListView, self).get_queryset().order_by("-create_time")
        for post in posts:
            md = markdown.Markdown(extensions =['markdown.extensions.extra','markdown.extensions.codehilite','markdown.extensions.toc',])
            post.code = md.convert(post.code)
            post.toc = md.toc
        return posts



