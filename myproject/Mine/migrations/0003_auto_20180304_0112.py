# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2018-03-04 01:12
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Mine', '0002_auto_20180302_2138'),
    ]

    operations = [
        migrations.AlterField(
            model_name='projects',
            name='create_time',
            field=models.DateField(),
        ),
    ]
