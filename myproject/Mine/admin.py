from django.contrib import admin
from .models import Projects,tech_stack_tags,programming_language_tags,development_tools_tags,Algorithm


# Register your models here.
admin.site.register(Projects)
admin.site.register(tech_stack_tags)
admin.site.register(programming_language_tags)
admin.site.register(development_tools_tags)
admin.site.register(Algorithm)