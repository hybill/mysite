# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from django.urls import reverse

from django.contrib.auth.models import User

import markdown
from django.utils.html import strip_tags


# Create your models here.
class Test(models.Model):
    name = models.CharField(max_length=20)
    age = models.CharField(max_length=10, default='30')


class Test2(models.Model):
    testName = models.CharField(max_length=20, default='2017')


class Category(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Post(models.Model):
    ishide = models.BooleanField(default=False)
    title = models.CharField(max_length=100)

    def __str__(self):
        return self.title

    body = models.TextField()

    created_time = models.DateTimeField()
    modified_time = models.DateTimeField()
    excerpt = models.CharField(max_length=200, blank=True)

    category = models.ForeignKey('Category', on_delete=models.PROTECT)
    tags = models.ManyToManyField(Tag, blank=True)

    author = models.ForeignKey(User)

    def get_absolute_url(self):
        return reverse('mysite:detail', kwargs={'pk': self.pk})

    class Meta:
        ordering = ['-created_time', 'title']

    views = models.PositiveIntegerField(default=0)

    def increase_views(self):
        self.views += 1
        self.save(update_fields=['views'])

    def save(self, *args, **kwargs):
        if not self.excerpt:
            md = markdown.Markdown(extensions=[
                'markdown.extensions.extra',
                'markdown.extensions.codehilite',
            ])
            # 先将 Markdown 文本渲染成 HTML 文本
            # strip_tags 去掉 HTML 文本的全部 HTML 标签
            # 从文本摘取前 54 个字符赋给 excerpt
            self.excerpt = strip_tags(md.convert(self.body))[:54]
        # 调用父类的 save 方法将数据保存到数据库中
        super(Post, self).save(*args, **kwargs)
