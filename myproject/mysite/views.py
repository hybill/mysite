# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render,get_object_or_404
from django.views.generic import ListView,DetailView
from django.http import HttpResponse
from .models import Post,Category,Tag
from comments.forms import CommentForm
# from django.core.paginator import paginator,EmptyPage,PageNotAnInteger
import markdown
from django.utils.text import slugify
from markdown.extensions.toc import TocExtension
# Create your views here.

class IndexView(ListView):
    model = Post
    template_name = 'mysite/index.html'
    context_object_name = 'post_list'
    paginate_by = 10
    def get_queryset(self):
        return super(IndexView,self).get_queryset().filter(ishide = False)
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        paginator = context.get('paginator')
        page = context.get('page_obj')
        is_paginated = context.get('is_paginated')
        pagination_data = self.pagination_data(paginator, page, is_paginated)
        context.update(pagination_data)
        return context

    def pagination_data(self, paginator, page, is_paginated):
        if not is_paginated:
            return {}
        left = []
        right = []
        left_has_more = False
        right_has_more = False
        first = False
        last = False
        page_number = page.number
        total_pages = paginator.num_pages
        page_range = paginator.page_range
        if page_number == 1:
            # 如果用户请求的是第一页的数据，那么当前页左边的不需要数据，因此 left=[]（已默认为空）。
            # 此时只要获取当前页右边的连续页码号，
            # 比如分页页码列表是 [1, 2, 3, 4]，那么获取的就是 right = [2, 3]。
            # 注意这里只获取了当前页码后连续两个页码，你可以更改这个数字以获取更多页码。
            right = page_range[page_number:page_number + 2]

            # 如果最右边的页码号比最后一页的页码号减去 1 还要小，
            # 说明最右边的页码号和最后一页的页码号之间还有其它页码，因此需要显示省略号，通过 right_has_more 来指示。
            if right[-1] < total_pages - 1:
                right_has_more = True

            # 如果最右边的页码号比最后一页的页码号小，说明当前页右边的连续页码号中不包含最后一页的页码
            # 所以需要显示最后一页的页码号，通过 last 来指示
            if right[-1] < total_pages:
                last = True

        elif page_number == total_pages:
            # 如果用户请求的是最后一页的数据，那么当前页右边就不需要数据，因此 right=[]（已默认为空），
            # 此时只要获取当前页左边的连续页码号。
            # 比如分页页码列表是 [1, 2, 3, 4]，那么获取的就是 left = [2, 3]
            # 这里只获取了当前页码后连续两个页码，你可以更改这个数字以获取更多页码。
            left = page_range[(page_number - 3) if (page_number - 3) > 0 else 0:page_number - 1]

            # 如果最左边的页码号比第 2 页页码号还大，
            # 说明最左边的页码号和第 1 页的页码号之间还有其它页码，因此需要显示省略号，通过 left_has_more 来指示。
            if left[0] > 2:
                left_has_more = True

            # 如果最左边的页码号比第 1 页的页码号大，说明当前页左边的连续页码号中不包含第一页的页码，
            # 所以需要显示第一页的页码号，通过 first 来指示
            if left[0] > 1:
                first = True
        else:
            # 用户请求的既不是最后一页，也不是第 1 页，则需要获取当前页左右两边的连续页码号，
            # 这里只获取了当前页码前后连续两个页码，你可以更改这个数字以获取更多页码。
            left = page_range[(page_number - 3) if (page_number - 3) > 0 else 0:page_number - 1]
            right = page_range[page_number:page_number + 2]

            # 是否需要显示最后一页和最后一页前的省略号
            if right[-1] < total_pages - 1:
                right_has_more = True
            if right[-1] < total_pages:
                last = True

            # 是否需要显示第 1 页和第 1 页后的省略号
            if left[0] > 2:
                left_has_more = True
            if left[0] > 1:
                first = True

        data = {
            'left': left,
            'right': right,
            'left_has_more': left_has_more,
            'right_has_more': right_has_more,
            'first': first,
            'last': last,
        }

        return data
	
# def index(request):
# 	# return HttpResponse('welcome to my blog!')
# 	# return render(request,'mysite/index.html',context ={
# 	# 	'title':'my site home page',
# 	# 	'welcome':'welcome to my site'
# 	# 	})
# 	post_list = Post.objects.all().order_by('-created_time')
# 	return render(request,'mysite/index.html',context = {'post_list':post_list})

# def detail(request,pk):
# 	post = get_object_or_404(Post,pk = pk)
# 	post.body = markdown.markdown(post.body,extensions = [
# 		'markdown.extensions.extra',
# 		'markdown.extensions.codehilite',
# 		'markdown.extensions.toc',])
# 	return render(request, 'mysite/detail.html', context={'post': post})

# def detail(request,pk):
# 	post = get_object_or_404(Post,pk = pk)
# 	# +1
# 	post.increase_views()
# 	post.body = markdown.markdown(post.body,
# 		extensions = [
# 		'markdown.extensions.extra',
# 		'markdown.extensions.codehilite',
# 		'markdown.extensions.toc',
# 		])
# 	form = CommentForm()
# 	comment_list = post.comment_set.all()
# 	context = {
# 	'post':post,
# 	'form':form,
# 	'comment_list':comment_list
# 	}
# 	return render(request,'mysite/detail.html',context = context)

class PostDetailView(DetailView):
    model = Post
    template_name = 'mysite/detail.html'
    context_object_name = 'post'
    def get_queryset(self):
    	return super(PostDetailView,self).get_queryset().filter(ishide = False)
    def get(self,request,*args,**kwargs):
    	response = super(PostDetailView,self).get(request,*args,**kwargs)
    	self.object.increase_views()
    	return response
    def get_object(self, queryset=None):
        post = super(PostDetailView, self).get_object(queryset=None)
        md = markdown.Markdown(extensions =['markdown.extensions.extra','markdown.extensions.codehilite','markdown.extensions.toc',])
        post.body = md.convert(post.body)
        post.toc = md.toc
        return post
	
    def get_context_data(self, **kwargs):
        # 覆写 get_context_data 的目的是因为除了将 post 传递给模板外（DetailView 已经帮我们完成），
        # 还要把评论表单、post 下的评论列表传递给模板。
        context = super(PostDetailView, self).get_context_data(**kwargs)
        form = CommentForm()
        comment_list = self.object.comment_set.all()
        context.update({
            'form': form,
            'comment_list': comment_list
        })
        return context

# def archives(request, year, month):
#     post_list = Post.objects.filter(created_time__year=year,
#                                     created_time__month=month
#                                     ).order_by('-created_time')
#     return render(request, 'mysite/index.html', context={'post_list': post_list})

class ArchivesView(ListView):
	model = Post
	template_name = 'mysite/index.html'
	context_object_name = 'post_list'
	def get_queryset(self):
		year = self.kwargs.get('year')
		month = self.kwargs.get('month')
		return super(ArchivesView,self).get_queryset().filter(created_time__year = year,created_time__month = month,ishide = False)



# def category(request, pk):
#     # 记得在开始部分导入 Category 类
#     cate = get_object_or_404(Category, pk=pk)
#     post_list = Post.objects.filter(category=cate).order_by('-created_time')
#     return render(request, 'mysite/index.html', context={'post_list': post_list})

class CategoryView(ListView):
	model = Post
	template_name = 'mysite/index.html'
	context_object_name = 'post_list'
	def get_queryset(self):
		cate = get_object_or_404(Category,pk = self.kwargs.get('pk'))
		return super(CategoryView,self).get_queryset().filter(category = cate , ishide = False)

class TagView(ListView):
    model = Post
    template_name = 'mysite/index.html'
    context_object_name = 'post_list'
    def get_queryset(self):
        tag =  get_object_or_404(Tag, pk = self.kwargs.get('pk'))
        return super(TagView,self).get_queryset().filter(tags = tag , ishide = False)