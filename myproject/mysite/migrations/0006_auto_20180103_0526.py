# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2018-01-03 05:26
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mysite', '0005_auto_20171230_0030'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='category',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='mysite.Category'),
        ),
    ]
